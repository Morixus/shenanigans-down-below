﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopUpNPC : MonoBehaviour
{
    [SerializeField] private GameObject _textOverHead;
    [SerializeField] private Transform _transform;
    [SerializeField] private Transform _textOverTransform;
    [SerializeField] private int _posX;
    [SerializeField] private int _posY;

    private void Awake()
    {
        _transform = transform;
        _textOverTransform = _textOverHead.transform;
    }

    private void LateUpdate()
    {
        Vector3 screenpos = Camera.main.WorldToScreenPoint(_transform.position);
        screenpos.y += _posY;
        screenpos.x += _posX;
        _textOverTransform.position = screenpos;
    }
}