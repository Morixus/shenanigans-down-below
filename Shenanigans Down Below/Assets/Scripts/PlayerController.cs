﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject _guardPopUp;
    [SerializeField] private GameObject _shopkeeperPopUp;
    [SerializeField] private GameObject _barmanPopUp;
    [SerializeField] private GameObject _veteranPopUp;
    [SerializeField] private GameObject _barPopUp;
    [SerializeField] private GameObject _barExitPopUp;
    [SerializeField] private GameObject _back;
    [SerializeField] private GameObject _backVet;
    [SerializeField] private GameObject _menu;
    [SerializeField] private GameObject _journal;
    [SerializeField] private Animator _playeranimator;

    private Rigidbody2D _rb;

    public Vector2 movement = new Vector2();
    public float speed;
    public VectorValue startingPosition;

    private void Start()
    {
        transform.position = startingPosition.initialValue;
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && _menu.activeSelf == false)
        {
            _menu.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && _menu.activeSelf == true)
        {
            _menu.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.J) && _journal.activeSelf == false)
        {
            _journal.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.J) && _journal.activeSelf == true)
        {
            _journal.SetActive(false);
        }

        if (_back.activeSelf == true || _backVet.activeSelf == true || _menu.activeSelf == true || _journal.activeSelf == true)
        {
            speed = 0;
        }
        else if (_back.activeSelf == false || _backVet.activeSelf == false || _menu.activeSelf == false || _journal.activeSelf == false)
        {
            speed = 5;
        }
    }

    private void FixedUpdate()
    {
        GetInput();
        MoveCharacter(movement);
    }

    private void OnTriggerEnter2D(Collider2D collision)  //WIELKI DAWID Z PROGRAMISTYCZNEJ ODCHŁANI PATRZY, Palec prezesa wskazał
    {
        //collision.transform.GetChild(0); -- Nic ważnego, proszę nie dotykać
        if (collision.name == "Guard")
        {
            _guardPopUp.SetActive(true);
        }
        else if (collision.name == "Shopkeeper")
        {
            _shopkeeperPopUp.SetActive(true);
        }
        else if (collision.name == "Veteran")
        {
            _veteranPopUp.SetActive(true);
        }
        else if (collision.name == "Barman")
        {
            _barmanPopUp.SetActive(true);
        }
        
        else if (collision.name =="BarEntrance")
        {
            _barPopUp.SetActive(true);
        }
        else if (collision.name == "BarExit")
        {
            _barExitPopUp.SetActive(true);
        }
        else if (collision.name == "CantLeft")
        {

        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Guard")
        {
            _guardPopUp.SetActive(false);
        }
        else if (collision.name == "Shopkeeper")
        {
            _shopkeeperPopUp.SetActive(false);
        }
        else if (collision.name == "Veteran")
        {
            _veteranPopUp.SetActive(false);
        }
        else if (collision.name == "Barman")
        {
            _barmanPopUp.SetActive(false);
        }
        else if (collision.name == "BarEntrance")
        {
            _barPopUp.SetActive(false);
        }
        else if (collision.name == "BarExit")
        {
            _barExitPopUp.SetActive(false);
        }
    }

    private void GetInput()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
    }

    public void MoveCharacter(Vector2 movementVector)
    {
        movementVector.Normalize();
        _rb.velocity = movementVector * speed;
        if (_rb.velocity == new Vector2(5.0f, 0.0f))
        {
            _playeranimator.SetBool("isWalkingRight", true);
        }
        else
        {
            _playeranimator.SetBool("isWalkingRight", false);
        }

        if (_rb.velocity == new Vector2(-5.0f, 0.0f))
        {
            _playeranimator.SetBool("isWalkingLeft", true);
        }
        else
        {
            _playeranimator.SetBool("isWalkingLeft", false);
        }
    }
}