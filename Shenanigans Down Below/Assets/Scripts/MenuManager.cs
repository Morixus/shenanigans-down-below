﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private GameObject _settings;
    [SerializeField] private GameObject _menu;

    public void NewGame()
    {
        SceneManager.LoadSceneAsync("Cave", LoadSceneMode.Single);
    }

    public void Settings()
    {
        _settings.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Back()
    {
        _settings.SetActive(false);
    }

    public void ContinueGame()
    {
        _menu.SetActive(false);
    }

    public void Return()
    {
        SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
    }
}