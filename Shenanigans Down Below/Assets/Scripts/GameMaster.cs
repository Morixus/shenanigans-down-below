﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public static GameMaster instance;
    public int guardTalkCount;
    public int shopkeeperTalkCount;
    public int veteranTalkCount;
    public int barmanTalkCount;
    public bool firstTalkGuard;
    public bool lastTalkGuard;
    public bool firstTalkShopkeeper;
    public bool secondTalkShopkeeper;
    public bool thirdTalkShopkeeper;
    public bool firstTalkVeteran;
    public bool secondTalkVeteran;
    public bool thirdTalkVeteran;
    public bool fourthTalkVeteran;
    public bool firstTalkBarman;
    public bool secondTalkBarman;
    public bool thirdTalkBarman;
    public bool fourthTalkBarman;
    public bool fifthTalkBarman;
    public bool afterIntro;
    public bool riddleSolved;
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
}