﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class DialoguesManager : MonoBehaviour
{
    [SerializeField] private Dialogues _guard;
    [SerializeField] private TextMeshProUGUI _guardFirstText;
    [SerializeField] private TextMeshProUGUI _guardSecondText;
    [SerializeField] private TextMeshProUGUI _guardThirdText;
    [SerializeField] private TextMeshProUGUI _guardFourthText;
    [SerializeField] private TextMeshProUGUI _guardDialogues;
    [SerializeField] private Dialogues _shopkeeper;
    [SerializeField] private TextMeshProUGUI _shopkeeperFirstText;
    [SerializeField] private TextMeshProUGUI _shopkeeperSecondText;
    [SerializeField] private TextMeshProUGUI _shopkeeperThirdText;
    [SerializeField] private TextMeshProUGUI _shopkeeperFourthText;
    [SerializeField] private TextMeshProUGUI _shopkeeperDialogues;
    [SerializeField] private Dialogues _veteran;
    [SerializeField] private TextMeshProUGUI _veteranFirstText;
    [SerializeField] private TextMeshProUGUI _veteranSecondText;
    [SerializeField] private TextMeshProUGUI _veteranThirdText;
    [SerializeField] private TextMeshProUGUI _veteranFourthText;
    [SerializeField] private TextMeshProUGUI _veteranDialogues;
    [SerializeField] private Dialogues _barman;
    [SerializeField] private TextMeshProUGUI _barmanFirstText;
    [SerializeField] private TextMeshProUGUI _barmanSecondText;
    [SerializeField] private TextMeshProUGUI _barmanThirdText;
    [SerializeField] private TextMeshProUGUI _barmanFourthText;
    [SerializeField] private TextMeshProUGUI _barmanDialogues;
    [SerializeField] private GameObject _back;
    [SerializeField] private GameObject _backVet;
    [SerializeField] private GameObject _guardPopUp;
    [SerializeField] private GameObject _shopkeeperPopUp;
    [SerializeField] private GameObject _veteranPopUp;
    [SerializeField] private GameObject _barmanPopUp;
    [SerializeField] private UIManager _uiMan;

    private bool _guardCheck = false;
    private bool _shopkeeperCheck = false;
    private bool _veteranCheck = false;
    private bool _barmanCheck = false;

    private void Start()
    {
        _guard.SetTree("First");
        _shopkeeper.SetTree("BeforeGuardTalk");
        _veteran.SetTree("BeforeGuardTalk");
        _barman.SetTree("BeforeGuardTalk");
    }

    public void GuardChoice(int index)
    {
        if (index == 0 && _guard.GetCurrentTree() == "BeforeAllTalks")
        {
            index = 1;
        }
        if (_guard.GetChoices().Length != 0)
        {
            _guard.NextChoice(_guard.GetChoices()[index]);
            GuardDisplay();
        }
        else
        {
            GuardProgress();
        }
    }
    public void ShopkeeperChoice(int index)
    {
        if (index == 0 && _shopkeeper.GetCurrentTree() == "SecondBeforeGuardTalk")
        {
            index = 1;
        }
        if (_shopkeeper.GetChoices().Length != 0)
        {
            _shopkeeper.NextChoice(_shopkeeper.GetChoices()[index]);
            ShopkeeperDisplay();
        }
        else
        {
            ShopkeeperProgress();
        }
    }
    public void VeteranChoice(int index)
    {
        if (index == 0 && _veteran.GetCurrentTree() == "FirstTalkBeforeBeer")
        {
            index = 1;
        }
        if (_veteran.GetChoices().Length != 0)
        {
            _veteran.NextChoice(_veteran.GetChoices()[index]);
            VeteranDisplay();
        }
        else
        {
            VeteranProgress();
        }
    }
    public void BarmanChoice(int index)
    {
        if (index == 0 && _barman.GetCurrentTree() == "FirstTalkBeforeLetter")
        {
            index = 1;
        }
        if (_barman.GetChoices().Length != 0)
        {
            _barman.NextChoice(_barman.GetChoices()[index]);
            BarmanDisplay();
        }
        else
        {
            BarmanProgress();
        }
    }

    public void GuardFirst()
    {
        _guard.SetTree("First");
        _guardCheck = false;
        GuardDisplay();
    }
    public void ShopkeeperFirst()
    {
        _shopkeeper.SetTree("BeforeGuardTalk");
        _shopkeeperCheck = false;
        ShopkeeperDisplay();
    }
    public void VeteranFirst()
    {
        _veteran.SetTree("BeforeGuardTalk");
        _veteranCheck = false;
        VeteranDisplay();
    }
    public void BarmanFirst()
    {
        _barman.SetTree("BeforeGuardTalk");
        _barmanCheck = false;
        BarmanDisplay();
    }

    public void GuardSecond()
    {
        _guard.SetTree("BeforeAllTalks");
        _guardCheck = false;
        GuardDisplay();
    }
    public void ShopkeeperSecond()
    {
        _shopkeeper.SetTree("SecondBeforeGuardTalk");
        _shopkeeperCheck = false;
        ShopkeeperDisplay();
    }
    public void VeteranSecond()
    {
        _veteran.SetTree("FirstTalkBeforeBeer");
        _veteranCheck = false;
        VeteranDisplay();
    }
    public void BarmanSecond()
    {
        _barman.SetTree("FirstTalkBeforeLetter");
        _barmanCheck = false;
        BarmanDisplay();
    }

    public void GuardThird()
    {
        _guard.SetTree("AfterAllTalks");
        _guardCheck = false;
        GuardDisplay();
    }
    public void ShopkeeperThird()
    {
        _shopkeeper.SetTree("AfterGuardTalk");
        _shopkeeperCheck = false;
        ShopkeeperDisplay();
    }
    public void VeteranThird()
    {
        _veteran.SetTree("SecondTalkBeforeBeer");
        _veteranCheck = false;
        VeteranDisplay();
    }
    public void BarmanThird()
    {
        _barman.SetTree("SecondTalkBeforeLetter");
        _barmanCheck = false;
        BarmanDisplay();
    }

    public void ShopkeeperFourth()
    {
        _shopkeeper.SetTree("BeforeDeliveringLetter");
        _shopkeeperCheck = false;
        ShopkeeperDisplay();
    }
    public void BarmanFourth()
    {
        _barman.SetTree("AfterGettingLetter");
        _barmanCheck = false;
        BarmanDisplay();
    }
    public void VeteranFourth()
    {
        _veteran.SetTree("AfterGettingBeer");
        _veteranCheck = false;
        VeteranDisplay();
    }

    public void ShopkeeperFifth()
    {
        _shopkeeper.SetTree("AfterDeliveringLetter");
        _shopkeeperCheck = false;
        ShopkeeperDisplay();
    }
    public void BarmanFifth()
    {
        _barman.SetTree("AfterDeliveringLetter");
        _barmanCheck = false;
        BarmanDisplay();
    }

    public void GuardProgress()
    {
        _guard.Next();
        GuardDisplay();
    }
    public void ShopkeeperProgress()
    {
        _shopkeeper.Next();
        ShopkeeperDisplay();
    }
    public void VeteranProgress()
    {
        _veteran.Next();
        VeteranDisplay();
    }
    public void BarmanProgress()
    {
        _barman.Next();
        BarmanDisplay();
    }

    public void ShopkeeperDefault()
    {
        _shopkeeper.SetTree("Default");
        _shopkeeperCheck = false;
        ShopkeeperDisplay();
    }
    public void VeteranDefault()
    {
        _veteran.SetTree("Default");
        _veteranCheck = false;
        VeteranDisplay();
    }
    public void BarmanDefault()
    {
        _barman.SetTree("Default");
        _barmanCheck = false;
        BarmanDisplay();
    }

    public void GuardDisplay()
    {
        if (_guardCheck == true)
        {
            _back.SetActive(false);
        }
        else
        {
            _back.SetActive(true);
        }

        _guardDialogues.text = _guard.GetCurrentDialogue();

        if (_guard.GetChoices().Length != 0)
        {
            if (_guard.GetChoices().Length == 1)
            {
                _guardFirstText.text = _guard.GetChoices()[0];
                _guardFirstText.transform.parent.gameObject.SetActive(true);
                _guardSecondText.transform.parent.gameObject.SetActive(false);
                _guardThirdText.transform.parent.gameObject.SetActive(false);
                _guardFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_guard.GetChoices().Length == 2)
            {
                _guardFirstText.text = _guard.GetChoices()[0];
                _guardSecondText.text = _guard.GetChoices()[1];
                _guardFirstText.transform.parent.gameObject.SetActive(true);
                _guardSecondText.transform.parent.gameObject.SetActive(true);
                _guardThirdText.transform.parent.gameObject.SetActive(false);
                _guardFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_guard.GetChoices().Length == 3)
            {
                _guardFirstText.text = _guard.GetChoices()[0];
                _guardSecondText.text = _guard.GetChoices()[1];
                _guardThirdText.text = _guard.GetChoices()[2];
                _guardFirstText.transform.parent.gameObject.SetActive(true);
                _guardSecondText.transform.parent.gameObject.SetActive(true);
                _guardThirdText.transform.parent.gameObject.SetActive(true);
                _guardFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_guard.GetChoices().Length == 4)
            {
                _guardFirstText.text = _guard.GetChoices()[0];
                _guardSecondText.text = _guard.GetChoices()[1];
                _guardThirdText.text = _guard.GetChoices()[2];
                _guardFourthText.text = _guard.GetChoices()[3];
                _guardFirstText.transform.parent.gameObject.SetActive(true);
                _guardSecondText.transform.parent.gameObject.SetActive(true);
                _guardThirdText.transform.parent.gameObject.SetActive(true);
                _guardFourthText.transform.parent.gameObject.SetActive(true);
            }
        }
        else
        {
            _guardFirstText.text = "Continue";
            _guardSecondText.transform.parent.gameObject.SetActive(false);
            _guardThirdText.transform.parent.gameObject.SetActive(false);
            _guardFourthText.transform.parent.gameObject.SetActive(false);
        }

        if (_guard.End())
        {
            if (_uiMan._lastTalkGuard == true)
            {
                _uiMan.riddleSolved = true;
                SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
            }
            _guardCheck = true;
            _guardPopUp.SetActive(true);
        }
    }
    public void ShopkeeperDisplay()
    {
        if (_shopkeeperCheck == true)
        {
            _back.SetActive(false);
        }
        else
        {
            _back.SetActive(true);
        }

        _shopkeeperDialogues.text = _shopkeeper.GetCurrentDialogue();

        if (_shopkeeper.GetChoices().Length != 0)
        {
            if (_shopkeeper.GetChoices().Length == 1)
            {
                _shopkeeperFirstText.text = _shopkeeper.GetChoices()[0];
                _shopkeeperFirstText.transform.parent.gameObject.SetActive(true);
                _shopkeeperSecondText.transform.parent.gameObject.SetActive(false);
                _shopkeeperThirdText.transform.parent.gameObject.SetActive(false);
                _shopkeeperFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_shopkeeper.GetChoices().Length == 2)
            {
                _shopkeeperFirstText.text = _shopkeeper.GetChoices()[0];
                _shopkeeperSecondText.text = _shopkeeper.GetChoices()[1];
                _shopkeeperFirstText.transform.parent.gameObject.SetActive(true);
                _shopkeeperSecondText.transform.parent.gameObject.SetActive(true);
                _shopkeeperThirdText.transform.parent.gameObject.SetActive(false);
                _shopkeeperFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_shopkeeper.GetChoices().Length == 3)
            {
                _shopkeeperFirstText.text = _shopkeeper.GetChoices()[0];
                _shopkeeperSecondText.text = _shopkeeper.GetChoices()[1];
                _shopkeeperThirdText.text = _shopkeeper.GetChoices()[2];
                _shopkeeperFirstText.transform.parent.gameObject.SetActive(true);
                _shopkeeperSecondText.transform.parent.gameObject.SetActive(true);
                _shopkeeperThirdText.transform.parent.gameObject.SetActive(true);
                _shopkeeperFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_shopkeeper.GetChoices().Length == 4)
            {
                _shopkeeperFirstText.text = _shopkeeper.GetChoices()[0];
                _shopkeeperSecondText.text = _shopkeeper.GetChoices()[1];
                _shopkeeperThirdText.text = _shopkeeper.GetChoices()[2];
                _shopkeeperFourthText.text = _shopkeeper.GetChoices()[3];
                _shopkeeperFirstText.transform.parent.gameObject.SetActive(true);
                _shopkeeperSecondText.transform.parent.gameObject.SetActive(true);
                _shopkeeperThirdText.transform.parent.gameObject.SetActive(true);
                _shopkeeperFourthText.transform.parent.gameObject.SetActive(true);
            }
        }
        else
        {
            _shopkeeperFirstText.text = "Continue";
            _shopkeeperSecondText.transform.parent.gameObject.SetActive(false);
            _shopkeeperThirdText.transform.parent.gameObject.SetActive(false);
            _shopkeeperFourthText.transform.parent.gameObject.SetActive(false);
        }

        if (_shopkeeper.End())
        {
            _shopkeeperCheck = true;
            _shopkeeperPopUp.SetActive(true);
        }
    }
    public void VeteranDisplay()
    {
        if (_veteranCheck == true)
        {
            _backVet.SetActive(false);
        }
        else
        {
            _backVet.SetActive(true);
        }

        _veteranDialogues.text = _veteran.GetCurrentDialogue();

        if (_veteran.GetChoices().Length != 0)
        {
            if (_veteran.GetChoices().Length == 1)
            {
                _veteranFirstText.text = _veteran.GetChoices()[0];
                _veteranFirstText.transform.parent.gameObject.SetActive(true);
                _veteranSecondText.transform.parent.gameObject.SetActive(false);
                _veteranThirdText.transform.parent.gameObject.SetActive(false);
                _veteranFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_veteran.GetChoices().Length == 2)
            {
                _veteranFirstText.text = _veteran.GetChoices()[0];
                _veteranSecondText.text = _veteran.GetChoices()[1];
                _veteranFirstText.transform.parent.gameObject.SetActive(true);
                _veteranSecondText.transform.parent.gameObject.SetActive(true);
                _veteranThirdText.transform.parent.gameObject.SetActive(false);
                _veteranFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_veteran.GetChoices().Length == 3)
            {
                _veteranFirstText.text = _veteran.GetChoices()[0];
                _veteranSecondText.text = _veteran.GetChoices()[1];
                _veteranThirdText.text = _veteran.GetChoices()[2];
                _veteranFirstText.transform.parent.gameObject.SetActive(true);
                _veteranSecondText.transform.parent.gameObject.SetActive(true);
                _veteranThirdText.transform.parent.gameObject.SetActive(true);
                _veteranFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_veteran.GetChoices().Length == 4)
            {
                _veteranFirstText.text = _veteran.GetChoices()[0];
                _veteranSecondText.text = _veteran.GetChoices()[1];
                _veteranThirdText.text = _veteran.GetChoices()[2];
                _veteranFourthText.text = _veteran.GetChoices()[3];
                _veteranFirstText.transform.parent.gameObject.SetActive(true);
                _veteranSecondText.transform.parent.gameObject.SetActive(true);
                _veteranThirdText.transform.parent.gameObject.SetActive(true);
                _veteranFourthText.transform.parent.gameObject.SetActive(true);
            }
        }
        else
        {
            _veteranFirstText.text = "Continue";
            _veteranSecondText.transform.parent.gameObject.SetActive(false);
            _veteranThirdText.transform.parent.gameObject.SetActive(false);
            _veteranFourthText.transform.parent.gameObject.SetActive(false);
        }

        if (_veteran.End())
        {
            _veteranCheck = true;
            _veteranPopUp.SetActive(true);
        }
    }
    public void BarmanDisplay()
    {
        if (_barmanCheck == true)
        {
            _back.SetActive(false);
        }
        else
        {
            _back.SetActive(true);
        }

        _barmanDialogues.text = _barman.GetCurrentDialogue();

        if (_barman.GetChoices().Length != 0)
        {
            if (_barman.GetChoices().Length == 1)
            {
                _barmanFirstText.text = _barman.GetChoices()[0];
                _barmanFirstText.transform.parent.gameObject.SetActive(true);
                _barmanSecondText.transform.parent.gameObject.SetActive(false);
                _barmanThirdText.transform.parent.gameObject.SetActive(false);
                _barmanFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_barman.GetChoices().Length == 2)
            {
                _barmanFirstText.text = _barman.GetChoices()[0];
                _barmanSecondText.text = _barman.GetChoices()[1];
                _barmanFirstText.transform.parent.gameObject.SetActive(true);
                _barmanSecondText.transform.parent.gameObject.SetActive(true);
                _barmanThirdText.transform.parent.gameObject.SetActive(false);
                _barmanFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_barman.GetChoices().Length == 3)
            {
                _barmanFirstText.text = _barman.GetChoices()[0];
                _barmanSecondText.text = _barman.GetChoices()[1];
                _barmanThirdText.text = _barman.GetChoices()[2];
                _barmanFirstText.transform.parent.gameObject.SetActive(true);
                _barmanSecondText.transform.parent.gameObject.SetActive(true);
                _barmanThirdText.transform.parent.gameObject.SetActive(true);
                _barmanFourthText.transform.parent.gameObject.SetActive(false);
            }
            else if (_barman.GetChoices().Length == 4)
            {
                _barmanFirstText.text = _barman.GetChoices()[0];
                _barmanSecondText.text = _barman.GetChoices()[1];
                _barmanThirdText.text = _barman.GetChoices()[2];
                _barmanFourthText.text = _barman.GetChoices()[3];
                _barmanFirstText.transform.parent.gameObject.SetActive(true);
                _barmanSecondText.transform.parent.gameObject.SetActive(true);
                _barmanThirdText.transform.parent.gameObject.SetActive(true);
                _barmanFourthText.transform.parent.gameObject.SetActive(true);
            }
        }
        else
        {
            _barmanFirstText.text = "Continue";
            _barmanSecondText.transform.parent.gameObject.SetActive(false);
            _barmanThirdText.transform.parent.gameObject.SetActive(false);
            _barmanFourthText.transform.parent.gameObject.SetActive(false);
        }

        if (_barman.End())
        {
            _barmanCheck = true;
            _barmanPopUp.SetActive(true);
        }
    }
}