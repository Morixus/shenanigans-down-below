﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class SwitcherBG : MonoBehaviour
{
    [SerializeField] private Animator _intro;
    [SerializeField] private GameObject _switchBG;
    private Scene _scene;

    public bool afterIntro;

    private void Start()
    {
        _scene = SceneManager.GetActiveScene();
        if (_scene.name == "Cave")
        {
            StartCoroutine(Intro());
        }
        
        afterIntro = GameMaster.instance.afterIntro;
        _intro.SetBool("AfterIntro", afterIntro);
    }

    private void Update()
    {
        GameMaster.instance.afterIntro = afterIntro;
    }

    private IEnumerator Intro()
    {
        _switchBG.SetActive(true);

        yield return new WaitForSeconds(12f);
        _intro.SetBool("AfterIntro", true);
        afterIntro = _intro.GetBool("AfterIntro");
    }
}