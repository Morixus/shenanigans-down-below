﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScaneTransformerBar : MonoBehaviour
{
    public string scenaToLoad;
    public Vector2 playerPosition;
    public VectorValue playerStorage;
    public GameObject fadeInPanel;
    public GameObject fadeOutPanel;
    public float fadeWait;
    public bool canGo = false;

    private void Awake()
    {
        if (fadeInPanel != null)
        {
            GameObject panel = Instantiate(fadeInPanel, Vector3.zero, Quaternion.identity) as GameObject;
            Destroy(panel, 1);
        }
    }
    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.F) && canGo == true)
        {

            playerStorage.initialValue = playerPosition;
            StartCoroutine(FadeCo()); // <----------- important
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
              canGo = true;
}
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            canGo = false;
        }
    }

    public IEnumerator FadeCo()
    {
        if (fadeOutPanel != null)
        {
            Instantiate(fadeOutPanel, Vector3.zero, Quaternion.identity);
            yield return new WaitForSeconds(fadeWait);
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scenaToLoad);
            while (!asyncOperation.isDone)
            {
                yield return null;
            }
        }

    }





}
