﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _guardPopUp;
    [SerializeField] private GameObject _shopkeeperPopUp;
    [SerializeField] private GameObject _barmanPopUp;
    [SerializeField] private GameObject _veteranPopUp;
    [SerializeField] private GameObject _barPopUp;
    [SerializeField] private GameObject _barExitPopUp;
    [SerializeField] private DialoguesManager _lines;
    [SerializeField] private GameObject _back;
    [SerializeField] private GameObject _backVet;
    [SerializeField] private GameObject _riddle;

    public bool _firstTalkGuard = false;
    public bool _lastTalkGuard = false;
    public bool _firstTalkShopkeeper = false;
    public bool _secondTalkShopkeeper = false;
    public bool _thirdTalkShopkeeper = false;
    public bool _firstTalkVeteran = false;
    public bool _secondTalkVeteran = false;
    public bool _thirdTalkVeteran = false;
    public bool _fourthTalkVeteran = false;
    public bool _firstTalkBarman = false;
    public bool _secondTalkBarman = false;
    public bool _thirdTalkBarman = false;
    public bool _fourthTalkBarman = false;
    public bool _fifthTalkBarman = false;
    public bool afterIntro;
    public bool riddleSolved = false;

    private void Start()
    {
        _firstTalkGuard = GameMaster.instance.firstTalkGuard;
        _lastTalkGuard = GameMaster.instance.lastTalkGuard;
        _firstTalkShopkeeper = GameMaster.instance.firstTalkShopkeeper;
        _secondTalkShopkeeper = GameMaster.instance.secondTalkShopkeeper;
        _thirdTalkShopkeeper = GameMaster.instance.thirdTalkShopkeeper;
        _firstTalkVeteran = GameMaster.instance.firstTalkVeteran;
        _secondTalkVeteran = GameMaster.instance.secondTalkVeteran;
        _thirdTalkVeteran = GameMaster.instance.thirdTalkVeteran;
        _fourthTalkVeteran = GameMaster.instance.fourthTalkVeteran;
        _firstTalkBarman = GameMaster.instance.firstTalkBarman;
        _secondTalkBarman = GameMaster.instance.secondTalkBarman;
        _thirdTalkBarman = GameMaster.instance.thirdTalkBarman;
        _fourthTalkBarman = GameMaster.instance.fourthTalkBarman;
        _fifthTalkBarman = GameMaster.instance.fifthTalkBarman;
        afterIntro = GameMaster.instance.afterIntro;
        riddleSolved = GameMaster.instance.riddleSolved;

        if (_firstTalkGuard == true)
        {
            _riddle.SetActive(true);
        }
    }

    private void Update()
    {
        //Guard
        //AfterAllTalks
        if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _guardPopUp.activeSelf == true && _firstTalkGuard == true && _thirdTalkShopkeeper == true && _fourthTalkVeteran == true && _fifthTalkBarman == true)
        {
            _lines.GuardThird();
            _guardPopUp.SetActive(false);
            _lastTalkGuard = true;
        }
        //BeforeAllTalks
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _guardPopUp.activeSelf == true && _firstTalkGuard == true)
        {
            _lines.GuardSecond();
            _guardPopUp.SetActive(false);
        }
        //First
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _guardPopUp.activeSelf == true && _firstTalkGuard == false)
        {
            _lines.GuardFirst();
            _guardPopUp.SetActive(false);
            _firstTalkGuard = true;
            _riddle.SetActive(true);
        }

        //Shopkeeper
        //AfterDeliveringLetter
        if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _shopkeeperPopUp.activeSelf == true && _secondTalkShopkeeper == true && _fourthTalkBarman == true)
        {
            _lines.ShopkeeperFifth();
            _shopkeeperPopUp.SetActive(false);
            _thirdTalkShopkeeper = true;
        }
        //BeforeDeliveringLetter
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _shopkeeperPopUp.activeSelf == true && _secondTalkShopkeeper == true && _secondTalkBarman == false)
        {
            _lines.ShopkeeperFourth();
            _shopkeeperPopUp.SetActive(false);
        }
        //AfterGuardTalk
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _shopkeeperPopUp.activeSelf == true && _firstTalkGuard == true)
        {
            _lines.ShopkeeperThird();
            _shopkeeperPopUp.SetActive(false);
            _secondTalkShopkeeper = true;
        }
        //SecondBeforeGuardTalk
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _shopkeeperPopUp.activeSelf == true && _firstTalkShopkeeper == true && _firstTalkGuard == false)
        {
            _lines.ShopkeeperSecond();
            _shopkeeperPopUp.SetActive(false);
        }
        //BeforeGuardTalk
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _shopkeeperPopUp.activeSelf == true && _firstTalkGuard == false)
        {
            _lines.ShopkeeperFirst();
            _shopkeeperPopUp.SetActive(false);
            _firstTalkShopkeeper = true;
        }

        //Veteran
        //Default
        if (Input.GetKeyDown(KeyCode.F) && _backVet.activeSelf == false && _veteranPopUp.activeSelf == true && _fourthTalkVeteran == true)
        {
            _lines.VeteranDefault();
            _veteranPopUp.SetActive(false);
        }
        //AfterGettingBeer
        else if (Input.GetKeyDown(KeyCode.F) && _backVet.activeSelf == false && _veteranPopUp.activeSelf == true && _secondTalkVeteran == true && _fifthTalkBarman == true)
        {
            _lines.VeteranFourth();
            _veteranPopUp.SetActive(false);
            _fourthTalkVeteran = true;
        }
        //SecondTalkBeforeBeer
        else if (Input.GetKeyDown(KeyCode.F) && _backVet.activeSelf == false && _veteranPopUp.activeSelf == true && _secondTalkVeteran == true)
        {
            _lines.VeteranThird();
            _veteranPopUp.SetActive(false);
            _thirdTalkVeteran = true;
        }
        //FirstTalkBeforeBeer
        else if (Input.GetKeyDown(KeyCode.F) && _backVet.activeSelf == false && _veteranPopUp.activeSelf == true && _firstTalkGuard == true)
        {
            _lines.VeteranSecond();
            _veteranPopUp.SetActive(false);
            _secondTalkVeteran = true;
        }
        //BeforeGuardTalk
        else if (Input.GetKeyDown(KeyCode.F) && _backVet.activeSelf == false && _veteranPopUp.activeSelf == true && _firstTalkGuard == false)
        {
            _lines.VeteranFirst();
            _veteranPopUp.SetActive(false);
            _firstTalkVeteran = true;
        }

        //Barman
        //Default
        if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _barmanPopUp.activeSelf == true && (_fifthTalkBarman == true || (_fourthTalkBarman == true && _thirdTalkShopkeeper == false)))
        {
            _lines.BarmanDefault();
            _barmanPopUp.SetActive(false);
        }
        //AfterDeliveringLetter
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _barmanPopUp.activeSelf == true && _fourthTalkBarman == true && _thirdTalkShopkeeper == true)
        {
            _lines.BarmanFifth();
            _barmanPopUp.SetActive(false);
            _fifthTalkBarman = true;
        }
        //AfterGettingLetter
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _barmanPopUp.activeSelf == true && _secondTalkShopkeeper == true)
        {
            _lines.BarmanFourth();
            _barmanPopUp.SetActive(false);
            _fourthTalkBarman = true;
        }
        //SecondTalkBeforeLetter
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _barmanPopUp.activeSelf == true && _secondTalkBarman == true)
        {
            _lines.BarmanThird();
            _barmanPopUp.SetActive(false);
            _thirdTalkBarman = true;
        }
        //FirstTalkBeforeLetter
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _barmanPopUp.activeSelf == true && _firstTalkGuard == true && _secondTalkShopkeeper == false)
        {
            _lines.BarmanSecond();
            _barmanPopUp.SetActive(false);
            _secondTalkBarman = true;
        }
        //BeforeGuardTalk
        else if (Input.GetKeyDown(KeyCode.F) && _back.activeSelf == false && _barmanPopUp.activeSelf == true && _firstTalkGuard == false)
        {
            _lines.BarmanFirst();
            _barmanPopUp.SetActive(false);
            _firstTalkBarman = true;
        }

        //Locations
        if (Input.GetKeyDown(KeyCode.F) && _barPopUp.activeSelf == true)
        {
            _barPopUp.SetActive(false);
            SceneManager.LoadScene("Bar", LoadSceneMode.Single);
        }
        
        if (Input.GetKeyDown(KeyCode.F) && _barExitPopUp.activeSelf == true)
        {
            _barExitPopUp.SetActive(false);
            SceneManager.LoadScene("Town", LoadSceneMode.Single);
            Debug.Log("TEST");
        }
        SaveVar();
    }

    public void SaveVar()
    {
        GameMaster.instance.firstTalkGuard = _firstTalkGuard;
        GameMaster.instance.lastTalkGuard = _lastTalkGuard;
        GameMaster.instance.firstTalkShopkeeper = _firstTalkShopkeeper;
        GameMaster.instance.secondTalkShopkeeper = _secondTalkShopkeeper;
        GameMaster.instance.thirdTalkShopkeeper = _thirdTalkShopkeeper;
        GameMaster.instance.firstTalkVeteran = _firstTalkVeteran;
        GameMaster.instance.secondTalkVeteran = _secondTalkVeteran;
        GameMaster.instance.thirdTalkVeteran = _thirdTalkVeteran;
        GameMaster.instance.fourthTalkVeteran = _fourthTalkVeteran;
        GameMaster.instance.firstTalkBarman = _firstTalkBarman;
        GameMaster.instance.secondTalkBarman = _secondTalkBarman;
        GameMaster.instance.thirdTalkBarman = _thirdTalkBarman;
        GameMaster.instance.fourthTalkBarman = _fourthTalkBarman;
        GameMaster.instance.fifthTalkBarman = _fifthTalkBarman;
        GameMaster.instance.afterIntro = afterIntro;
        GameMaster.instance.riddleSolved = riddleSolved;
    }
}