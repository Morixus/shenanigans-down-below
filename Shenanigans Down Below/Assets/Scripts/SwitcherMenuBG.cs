﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitcherMenuBG : MonoBehaviour
{
    [SerializeField] private Animator _outro;
    [SerializeField] private GameObject _switchMenuBG;
    [SerializeField] private GameObject[] _menu;
    private Scene _scene;

    public bool riddleSolved;

    private void Start()
    {
        riddleSolved = GameMaster.instance.riddleSolved;
        _scene = SceneManager.GetActiveScene();
        if (_scene.name == "MainMenu" && riddleSolved == true)
        {
            StartCoroutine(Outro());
        }

        _outro.SetBool("RiddleSolved", riddleSolved);
    }

    private void Update()
    {
        GameMaster.instance.riddleSolved = riddleSolved;
    }

    private IEnumerator Outro()
    {
        _menu[0].SetActive(false);
        _menu[1].SetActive(false);
        _menu[2].SetActive(false);
        _switchMenuBG.SetActive(true);

        yield return new WaitForSeconds(12f);
        _outro.SetBool("RiddleSolved", false);
        riddleSolved = _outro.GetBool("RiddleSolved");
        _switchMenuBG.SetActive(false);
        _menu[0].SetActive(true);
        _menu[1].SetActive(true);
        _menu[2].SetActive(true);
    }
}