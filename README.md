# Shenanigans Down Below

Project made by 6 people. Adventure game about leprechaun
who got stuck in other fraction territory and have to cross the border. To do
that he needs to help other leprechauns at the border.

# Kacper Lewandowicz:
*  Main menu
*  UI
*  Interaction with NPC's
*  Character movement
*  Dialogue system
*  Implementation of animations

**Script making text pop up above NPC's heads.**

    private void Awake()
    {
        _transform = transform;
        _textOverTransform = _textOverHead.transform;
    }

    private void LateUpdate()
    {
        Vector3 screenpos = Camera.main.WorldToScreenPoint(_transform.position);
        screenpos.y += _posY;
        screenpos.x += _posX;
        _textOverTransform.position = screenpos;
    }

**Resolution settings.**

    private void Start()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions(); // Clearing all options in resolutionDropdown

        List<string> options = new List<string>(); // Creating List of resolution options

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++) // looping thru every option in resolution array
        {
            string option = resolutions[i].width + " x " + resolutions[i].height; // Creating string for every resolution
            options.Add(option); // Adding resolution string to list of options

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options); // Adding option list to resolutionDropdown
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

# Jakub Czarnecki:
*  moving between location
*  animations of transition between locations

**Changing scene by fading into black and from black to another scene.**

    public IEnumerator FadeCo()
    {
        if (fadeOutPanel != null)
        {
            Instantiate(fadeOutPanel, Vector3.zero, Quaternion.identity);
            yield return new WaitForSeconds(fadeWait);
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scenaToLoad);
        while(!asyncOperation.isDone)
        {
            yield return null;
        }
    }
